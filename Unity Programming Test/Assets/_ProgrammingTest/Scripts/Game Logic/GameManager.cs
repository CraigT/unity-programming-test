﻿using UnityEngine;
using UnityEngine.UI;

/********************************************
*
*	Core game logic class that handles
*	game state.
*
********************************************/

public class GameManager : MonoBehaviour {

	private static GameManager instance;
	public static GameManager Instance {
		get { if (instance == null) instance = FindObjectOfType<GameManager>(); return instance; }
	}

	public GameObject gameEntitiesParent;
	public LevelLayoutController layoutController;
	public BrickManager brickManager;
	public PaddleController paddleController;

	public Text livesLabel;

	public int startLivesCount = 3;
	private int livesCount;

	private bool hasGameStarted;

	private ScreenOrientation orientation;

	private void Start () {
		instance = this;
		gameEntitiesParent.SetActive(false);
	}

	private void Update() {
		if (hasGameStarted && orientation != Screen.orientation) {
			gameEntitiesParent.SetActive(false);
			StartGame();
		}

		if (hasGameStarted && brickManager.AreAllBricksReturned()) {
			hasGameStarted = false;
			gameEntitiesParent.SetActive(false);
			UIManager.Instance.ShowState(UIState.Victory);
		}
	}

	public void StartGame() {
		ResetLives();
		UIManager.Instance.ShowState(UIState.HUD);
		ScoreManager.Instance.Score = 0;

		orientation = Screen.orientation;

		paddleController.ResetPaddle();

		BallManager.Instance.ResetBallManager();
		BallManager.Instance.SpawnBall();

		layoutController.LayoutLevel();
		brickManager.LayoutBricks(layoutController);

		gameEntitiesParent.SetActive(true);
		hasGameStarted = true;
	}

	public void OnPlayerWasKilled() {
		if (hasGameStarted) {
			livesCount--;
			UpdateLivesLabel();

			if (livesCount <= 0) {
				hasGameStarted = false;
				gameEntitiesParent.SetActive(false);
				UIManager.Instance.ShowState(UIState.GameOver);
			} else {
				BallManager.Instance.SpawnBall();
			}
		}
	}

	public void ResetLives() {
		livesCount = startLivesCount;
		UpdateLivesLabel();
	}

	private void UpdateLivesLabel() {
		livesLabel.text = "Lives: " + livesCount;
	}
}
