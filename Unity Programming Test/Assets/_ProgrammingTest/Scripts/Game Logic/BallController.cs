﻿using UnityEngine;

/********************************************
*
*	Class responsible for handling the
*	behaviour of the ball.
*
********************************************/

public class BallController : PooledObject {

	public new Rigidbody rigidbody;
	private Transform originalParent;

	private void Awake() {
		originalParent = transform.parent;
	}

	public void ParentToTarget(Transform _target) {
		rigidbody.isKinematic = true;
		transform.SetParent(_target);
	}

	public void ReturnToOriginalParent() {
		transform.SetParent(originalParent);
		rigidbody.isKinematic = false;
	}

	public void Launch(Vector3 _force) {
		ReturnToOriginalParent();
		rigidbody.AddForce(_force, ForceMode.Impulse);
	}
}
