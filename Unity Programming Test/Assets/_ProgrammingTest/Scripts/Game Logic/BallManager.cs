﻿using UnityEngine;
using System.Collections.Generic;

/********************************************
*
*	Class for handling ball spawning logic
*
********************************************/

public class BallManager : ObjectPool {

	private static BallManager instance;
	public static BallManager Instance {
		get { if (instance == null) instance = FindObjectOfType<BallManager>(); return instance; }
	}

	public PaddleController paddleController;

	private List<BallController> ballControllers;

	public void ResetBallManager() {
		if (ballControllers != null) {
			for (int i = 0; i < ballControllers.Count; i++) {
				ballControllers[i].ReturnToPool();
			}

			ballControllers.Clear();
		}
	}

	public void SpawnBall() {
		if (ballControllers == null) {
			ballControllers = new List<BallController>();
		}

		BallController ball = null;
        if (ballControllers.Count == 0) {
			ball = GetObjectOfType(prefabs[0]).GetComponent<BallController>();
            paddleController.HoldBall(ball);
		} else {
			ball = GetObjectOfType(prefabs[0]).GetComponent<BallController>();
			ball.transform.position = paddleController.ballRestLocation.position;
			ball.Launch(paddleController.CalculateLaunchForce());
		}

		ballControllers.Add(ball);
	}

	public void OnBallOutOfBounds(BallController _ball) {
		ballControllers.Remove(_ball);
		_ball.ReturnToPool();
		if (ballControllers.Count == 0) {
			GameManager.Instance.OnPlayerWasKilled();
		}
	}

}
