﻿using UnityEngine;

/********************************************
*
*	Class for laying out the walls and 
*	player position, and defining the 
*	play area.
*
********************************************/

public class LevelLayoutController : MonoBehaviour {

	[Header("Parameters")]
	public float playerHeight;		/* How far above the floor the player's paddle should sit (world units). */

	[Header("Scene References")]
	public Transform ceiling;		
	public Transform floor;			
	public Transform leftWall;
	public Transform rightWall;
	public Transform playerPaddle;

	public Camera targetCamera;		

	private Rect gameExtents;		/* Rect representing the world-space extents of the game space. */
	public Rect GameExtents {
		get { return gameExtents; }
	}

	public void LayoutLevel() {
		Vector2 extentMin = Vector2.zero;
		Vector2 extentMax = Vector2.zero;

		// For each wall, transform the center of the respective side of the screen to world space. This point represents
		// a good place to put each wall to utilize the full size of the screen, and also serves as a point in our extents

		// Ceiling first
		Vector3 screenPos = new Vector3(Screen.width * 0.5f, Screen.height, -targetCamera.transform.position.z);
		Vector3 worldPos = targetCamera.ScreenToWorldPoint(screenPos);
		ceiling.position = worldPos;
		extentMax.y = worldPos.y;

		// Floor next
		screenPos.y = 0;
		worldPos = targetCamera.ScreenToWorldPoint(screenPos);
		floor.position = worldPos;
		extentMin.y = worldPos.y;

		// While we're doing the floor, we can set the player's position to be slightly above the floor
		worldPos.y += playerHeight;
		playerPaddle.position = worldPos;

		// Now the right wall
		screenPos.x = Screen.width;
		screenPos.y = Screen.height * 0.5f;
		worldPos = targetCamera.ScreenToWorldPoint(screenPos);
		rightWall.position = worldPos;
		extentMax.x = worldPos.x;

		//And finally the left wall
		screenPos.x = 0;
		worldPos = targetCamera.ScreenToWorldPoint(screenPos);
		leftWall.position = worldPos;
		extentMin.x = worldPos.x;


		// Now that we have our extents, we can calculate our play area
		float width = extentMax.x - extentMin.x;
		float height = extentMax.y - extentMin.y;
		gameExtents.Set(extentMin.x, extentMin.y, width, height);

		// And now we can use those dimensions to scale our walls, to ensure we cover the entire area
		// We make the assumption here that the floor and ceiling should be the same width, ditto the right and left walls' heights
		Vector3 scale = ceiling.localScale;
		scale.x = width;
		ceiling.localScale = floor.localScale = scale;

		scale = rightWall.localScale;
		scale.y = height;
		rightWall.localScale = leftWall.localScale = scale;
	}
}
