﻿/********************************************
*
*	Simple brick rule for bricks that
*	can never be broken.
*
********************************************/

public class UnbreakableBrickRule : BrickRule {

	public override bool Process() {
		return false;
	}

}
