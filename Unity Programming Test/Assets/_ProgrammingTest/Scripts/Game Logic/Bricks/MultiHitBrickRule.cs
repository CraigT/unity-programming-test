﻿using UnityEngine;

/********************************************
*
*	Simple brick rule for bricks that
*	take more than one hit to break.
*
********************************************/

public class MultiHitBrickRule : BrickRule {

	public int hitsRequired;			
	public Color[] colourAtHitCount;

	private int hitCount;
	private Material material;

	public override void ResetRule() {
		base.ResetRule();
		hitCount = 0;
		if (material == null) {
			material = GetComponent<Renderer>().material;
		}
		material.color = colourAtHitCount[0];
	}

	public override bool Process() {
		hitCount++;
		bool finished = hitCount >= hitsRequired;

		if (!finished) {
			material.color = colourAtHitCount[hitCount];
		}

		return finished;
	}

}
