﻿using UnityEngine;

/********************************************
*
*	Simple brick rule for bricks that
*	award the player points when hit.
*
********************************************/

public class BrickScoreRule : BrickRule {

	public int scoreValue = 1;

	public override bool Process() {
		ScoreManager.Instance.Score += scoreValue;
		return true;
	}

}
