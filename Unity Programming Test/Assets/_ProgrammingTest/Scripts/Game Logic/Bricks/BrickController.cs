﻿using UnityEngine;
using System.Collections.Generic;

/********************************************
*
*	Logic controller for bricks
*
*	Handles rule processing when the ball
*	hits this brick
*
*	These get spawned by BrickManager
*
********************************************/

public class BrickController : PooledObject {

	public bool countForVictory = true;	/* When true, this brick must be destroyed for the player to win. */
	public List<BrickRule> brickRules;  /* List of rules that determine how bricks react to getting hit. */
	private bool destroyFlag;			/* We set this once all of the rules have succeeded, so we know to pool this object during the next frame. If we don't do this, the ball doesn't always bounce properly off blocks that are getting pooled. */

	// This object will be activated whenever it's pulled from the pool,
	// which is a good time to reset the logic of our rules.
	private void OnEnable() {
		for (int i = 0; i < brickRules.Count; i++) {
			brickRules[i].ResetRule();
		}
	}

	private void Update() {
		if (destroyFlag) {
			destroyFlag = false;
			ReturnToPool();
		}
	}

	private void OnCollisionEnter(Collision _collision) {
		if (!destroyFlag) {
			bool areAllRulesFinished = true;
			for (int i = 0; i < brickRules.Count; i++) {
				areAllRulesFinished &= brickRules[i].Process();
			}

			if (areAllRulesFinished) {
				destroyFlag = true;
			}
		}
	}

}
