﻿using UnityEngine;
using System.Collections.Generic;

/**************************************************
*
*	System for spawning bricks semi-intelligently
*
*	We use the size of the level to determine how
*	many bricks we can spawn, and then spawn them
*	with as much gap as we can
*
*	This class is also responsible for keeping 
*	track of the number of bricks in play.
*
***************************************************/

public class BrickManager : ObjectPool {

	[Header("Brick Layout Options")]
	public float[] brickProbability;        /* Likelihood of each prefab spawning. These should add up to 1.0f. */

	public float easyModeMultiplier = 3f;	/* How much larger to make the bricks for easy mode. */

	public float wallThickness = 0.5f;		/* How thick the walls are; ie how much space they take up in the game space. */
	public float minEmptySpaceAbovePlayer;  /* How much space we should leave above the player's paddle. */

	private int startBrickCount;            /* How many bricks we started with. This is used to determine when the player wins. */
	private int returnedBrickCount;         /* How many bricks have been returned. */

	private List<BrickController> spawnedBricks;

	public void LayoutBricks(LevelLayoutController _levelLayout) {

		startBrickCount = returnedBrickCount = 0;
		if (spawnedBricks == null) {
			spawnedBricks = new List<BrickController>();
		} else {
			for (int i = 0; i < spawnedBricks.Count; i++) {
				spawnedBricks[i].ReturnToPool();
			}

			spawnedBricks.Clear();
		}

		float scaleModifer = PlayerPrefs.GetInt("easy_mode") > 0 ? easyModeMultiplier : 1f;
		PlayerPrefs.SetInt("easy_mode", 0);

		Rect extents = _levelLayout.GameExtents;
		Vector3 brickScale = prefabs[0].transform.localScale * scaleModifer;

		// We calculate the space available to us as the total width of the camera's viewport
		// minus the width of the walls. We then calculate how many bricks can fit in there
		// and how much extra space we have left over, which we use to calculate the margins
		// we can use between the bricks.
		float useableWidth = extents.width - 2.0f * wallThickness;
		int bricksPerRow = Mathf.FloorToInt(useableWidth / brickScale.x);
		float remainingWidth = useableWidth - (bricksPerRow * brickScale.x);
		float horizontalMarginPerBrick = remainingWidth / (bricksPerRow + 1);

		float useableHeight = extents.height - 2.0f * wallThickness - minEmptySpaceAbovePlayer;
		int bricksPerColumn = Mathf.FloorToInt(useableHeight / brickScale.y);
		float remainingHeight = useableHeight - (bricksPerColumn * brickScale.y);
		float verticalMarginPerBrick = remainingHeight / (bricksPerColumn + 1);

		
		// Now that we have our spacing values, we can actually spawn and place the bricks
		for (int x = 0; x < bricksPerRow; x++) {
			for (int y = 0; y < bricksPerColumn; y++) {

				// First, we pick a prefab to spawn, based on the probabilities specified
				float result = Random.Range(0f, 1f);
				float probability = 0;
				int prefabIndex = 0;
				for (int i = 0; i < prefabs.Length; i++) {
					probability += brickProbability[i];
					if (result < probability) {
						prefabIndex = i;
						break;
					}
				}
				// Now we can actually spawn the brick
				GameObject brick = GetObjectOfType(prefabs[prefabIndex]);
				brick.transform.position = new Vector3(extents.x + wallThickness + horizontalMarginPerBrick + 0.5f * brickScale.x + (brickScale.x + horizontalMarginPerBrick) * x,
													   extents.y + wallThickness + minEmptySpaceAbovePlayer + verticalMarginPerBrick + 0.5f * brickScale.y + (brickScale.y + verticalMarginPerBrick) * y,
													   _levelLayout.ceiling.position.z);
				brick.transform.localScale = brickScale;

				BrickController brickController = brick.GetComponent<BrickController>();
				// Keep track of how many bricks need to be destroyed for the player to win
				if (brickController.countForVictory) {
					startBrickCount++;
				}

				spawnedBricks.Add(brickController);
			}
		}
	}

	public bool AreAllBricksReturned() {
		//Debug.Log(startBrickCount + " " + returnedBrickCount);
		return startBrickCount <= returnedBrickCount;
	}

	public override void ReturnObjectToPool(PooledObject _pooledObject) {
		base.ReturnObjectToPool(_pooledObject);

		if (startBrickCount > 0) {
			SpecialAbilityManager.Instance.OnBrickBroken(_pooledObject.transform.position);

			if ((_pooledObject as BrickController).countForVictory) {
				returnedBrickCount++;
			}
		}
	}

	public void ActivateEasyMode() {
		PlayerPrefs.SetInt("easy_mode", 1);
	}
}
