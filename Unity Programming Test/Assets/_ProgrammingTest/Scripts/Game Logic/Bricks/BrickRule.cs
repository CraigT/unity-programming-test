﻿using UnityEngine;

/********************************************
*
*	Stub class for brick rules
*
*	Rules are piecemeal logic checks
*	that chain together to determine
*	how bricks react to getting hit.
*
*	Their order is determined by their
*	order as specified in the BrickController
*	inspector.
*
********************************************/

public abstract class BrickRule : MonoBehaviour {

	public virtual void ResetRule() {}

	public abstract bool Process();
}
