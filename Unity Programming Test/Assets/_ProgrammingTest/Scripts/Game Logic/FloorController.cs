﻿using UnityEngine;

/********************************************
*
*	Class that reports to the Ball Manager
*	when a ball goes out of bounds.
*
********************************************/

public class FloorController : MonoBehaviour {

	private void OnTriggerEnter(Collider _collider) {
		BallController ball = _collider.gameObject.GetComponent<BallController>();
		if (ball != null) {
			BallManager.Instance.OnBallOutOfBounds(ball);
			return;
        }

		SpecialAbility ability = _collider.gameObject.GetComponent<SpecialAbility>();
		if (ability != null) {
			ability.ReturnToPool();
		}
	}

}
