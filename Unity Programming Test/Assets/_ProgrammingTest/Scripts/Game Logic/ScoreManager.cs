﻿using UnityEngine;
using UnityEngine.UI;

/********************************************
*
*	Simple singleton that handles
*	scoring logic.
*
********************************************/

public class ScoreManager : MonoBehaviour {

	private static ScoreManager instance;
	public static ScoreManager Instance {
		get { if (instance == null) instance = FindObjectOfType<ScoreManager>(); return instance; }
	}

	public Text scoreLabel;

	private int score;
	public int Score {
		get { return score; }
		set { score = value; UpdateScoreLabel(); }
	}

	private void UpdateScoreLabel() {
		scoreLabel.text = "Score: " + score;
	}
}
