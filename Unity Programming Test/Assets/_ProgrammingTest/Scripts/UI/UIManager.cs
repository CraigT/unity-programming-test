﻿using UnityEngine;

/********************************************
*
*	Really basic UI state manager.
*
********************************************/

public enum UIState {
	None,
	MainMenu,
	HUD,
	GameOver,
	Victory
}

public class UIManager : MonoBehaviour {

	private static UIManager instance;
	public static UIManager Instance {
		get { if (instance == null) instance = FindObjectOfType<UIManager>(); return instance; }
	}

	private void Start() {
		instance = this;

		for (int i = 1; i < uiStateObjects.Length; i++) {
			uiStateObjects[i].SetActive((UIState)i == defaultState);
		}

		activeState = defaultState;
	}

	public GameObject[] uiStateObjects;
	public UIState defaultState;

	private UIState activeState = UIState.None;

	public void ShowState(UIState _state) {
		if (_state != activeState) {
			if (activeState != UIState.None) {
				uiStateObjects[(int)activeState].SetActive(false);
            }

			activeState = _state;
			if (activeState != UIState.None) {
				uiStateObjects[(int)activeState].SetActive(true);
			}
		}
	}

	public void ShowDefaultState() {
		ShowState(defaultState);
	}
}
