﻿using UnityEngine;

/********************************************
*
*	Logic controller for handling player input
*	and moving the player's paddle.
*
********************************************/

public class PaddleController : MonoBehaviour {

	public Camera targetCamera;

	public Transform ballRestLocation;
	private BallController heldBall;

	public float launchForce = 20f;

	void Update () {
		Vector3 input = Vector3.zero;
		bool movePaddle = true;
		bool launch = false;

#if UNITY_EDITOR
		launch = Input.GetMouseButtonDown(0);
		input = Input.mousePosition;
#else
		if (Input.touchCount > 0) {
			input = Input.GetTouch(0).position;
			launch = Input.GetTouch(0).phase == TouchPhase.Began;
		} else {
			movePaddle = false;
		}
		
#endif

		if (movePaddle) {
			Vector3 position = targetCamera.ScreenToWorldPoint(input);
			transform.position = new Vector3(position.x, transform.position.y, transform.position.z);
		}

		if (heldBall != null && launch) {
			heldBall.Launch(CalculateLaunchForce());
			heldBall = null;
		}
	}

	public void ResetPaddle() {
		PaddleModule[] modules = GetComponents<PaddleModule>();
		if (modules != null) {
			for (int i = 0; i < modules.Length; i++) {
				Destroy(modules[i]);
			}
		}
	}

	public Vector3 CalculateLaunchForce() {
		return Vector3.Normalize(new Vector3(Random.Range(-1f, 1f), Random.Range(0.5f, 1f), 0)) * launchForce;
    }

	public bool HoldBall(BallController _ball) {
		if (heldBall == null) {
			heldBall = _ball;
			heldBall.ParentToTarget(transform);
			heldBall.transform.position = ballRestLocation.position;
			return true;
		}

		return false;
	}

	private void OnTriggerEnter(Collider _collider) {
		SpecialAbility ability = _collider.GetComponent<SpecialAbility>();
		if (ability != null) {
			ability.Activate();
		}
	}
}
