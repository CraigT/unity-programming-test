﻿using UnityEngine;

public class StickyPaddleModule : PaddleModule {

	public int catchesAllowed = 5;

	private int catches;

	protected override bool IsFinished() {
		return catches >= catchesAllowed;
	}

	protected void OnCollisionEnter(Collision _collision) {
		BallController ball = _collision.collider.GetComponent<BallController>();
		if (ball != null) {
			if (paddleController.HoldBall(ball)) {
				catches++;
			}
		}
	}
}
