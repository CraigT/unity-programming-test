﻿using UnityEngine;

public abstract class PaddleModule : MonoBehaviour {

	protected PaddleController paddleController;

	protected virtual void Start() {
		paddleController = GetComponent<PaddleController>();
	}

	protected virtual void Update() {
		if (IsFinished()) {
			Destroy(this);
		}
	}

	protected abstract bool IsFinished();
}
