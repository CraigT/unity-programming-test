﻿using UnityEngine;

public class MultiBallAbility : SpecialAbility {

	public int extraBallCount = 3;

	public override void Activate() {
		for (int i = 0; i < extraBallCount; i++) {
			BallManager.Instance.SpawnBall();
		}

		ReturnToPool();
	}

}
