﻿using UnityEngine;

/********************************************
*
*	Base class for special abilities.
*	These randomly drop from bricks when
*	broken.
*
********************************************/

public enum SpecialAbilityType {
	None,
	MultiBall,
	StickyPaddle
}

public abstract class SpecialAbility : PooledObject {

	public SpecialAbilityType abilityType;

	public abstract void Activate();
	
}
