﻿using UnityEngine;

/********************************************
*
*	Class that handles spawning logic
*	for special abilities
*
********************************************/

public class SpecialAbilityManager : ObjectPool {

	private static SpecialAbilityManager instance;
	public static SpecialAbilityManager Instance {
		get { if (instance == null) instance = FindObjectOfType<SpecialAbilityManager>(); return instance; }
	}

	public float[] abilityProbabilities;

	protected override void Start() {
		base.Start();
		instance = this;
	}

	public void OnBrickBroken(Vector3 _position) {
		float chance = Random.Range(0f, 1f);

		float probability = 0;
		for (int i = 0; i < abilityProbabilities.Length; i++) {
			probability += abilityProbabilities[i];
			if (chance <= probability) {
				SpawnAbility(i, _position);
				break;
			}
		}
	}

	private void SpawnAbility(int _index, Vector3 _position) {
		GameObject ability = GetObjectOfType(prefabs[_index]);
		ability.transform.position = _position;
		ability.GetComponent<Rigidbody>().velocity = Vector3.zero;
	}
}
