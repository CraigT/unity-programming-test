﻿using UnityEngine;

public class StickyPaddleAbility : SpecialAbility {

	private PaddleController paddle;

	protected void Start() {
		paddle = FindObjectOfType<PaddleController>();
	}

	public override void Activate() {
		paddle.gameObject.AddComponent<StickyPaddleModule>();
		ReturnToPool();
	}

}
