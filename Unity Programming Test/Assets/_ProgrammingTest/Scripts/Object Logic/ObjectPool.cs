﻿using UnityEngine;
using System.Collections.Generic;

/********************************************
*
*	Basic object pooling system.
*
********************************************/

public class ObjectPool : MonoBehaviour {

	[Header("Object Pool Settings")]
	public GameObject[] prefabs;

	protected Dictionary<string, Queue<PooledObject>> pool;
	protected bool isInitialized;

	protected virtual void Start () {
		Initialize();
	}

	// Set up the dictionary with the pre-specified prefabs
	protected virtual void Initialize() {
		if (!isInitialized) {
			pool = new Dictionary<string, Queue<PooledObject>>();

			for (int i = 0; i < prefabs.Length; i++) {
				if (prefabs[i] != null) {
					InitializeObjectOfType(prefabs[i]);
				}
			}

			isInitialized = true;
		}
	}

	// Set up a prefab queue in the pool
	protected virtual void InitializeObjectOfType(GameObject _prefab) {
		if (!pool.ContainsKey(_prefab.name)) {
			Queue<PooledObject> pooledObjects = new Queue<PooledObject>();
			pool.Add(_prefab.name, pooledObjects);
		}
	}

	// Generate an object for a queue
	protected virtual GameObject GenerateObjectOfType(GameObject _prefab, bool _suppressEnterEvent = false) {
		GameObject go = Instantiate(_prefab);
		go.transform.SetParent(transform, false);

		PooledObject po = go.GetComponent<PooledObject>();
		po.prefabName = _prefab.name;

		InitializeObjectOfType(_prefab);

		if (_suppressEnterEvent) {
			po.objectPool = this;
		} else {
			po.OnPoolEntered(this);
			pool[_prefab.name].Enqueue(po);
		}

		return go;
	}

	// Fetches an object from the queue if one's available, otherwise generates one
	public virtual GameObject GetObjectOfType(GameObject _prefab) {
		Initialize();
		GameObject go = null;
		if (!pool.ContainsKey(_prefab.name)) {
			InitializeObjectOfType(_prefab);
		}

		if (pool[_prefab.name].Count == 0) {
			go = GenerateObjectOfType(_prefab, true);
		} else {
			PooledObject po = pool[_prefab.name].Dequeue();
			po.OnPoolExited();
			go = po.gameObject;
		}

		return go;
	}

	// Returns and deactivates the pooled object, for use later
	public virtual void ReturnObjectToPool(PooledObject _pooledObject) {
		if (pool.ContainsKey(_pooledObject.prefabName)) {
			_pooledObject.OnPoolEntered(this);
			if (!pool[_pooledObject.prefabName].Contains(_pooledObject)) {
				pool[_pooledObject.prefabName].Enqueue(_pooledObject);
			}
		}
	}
}
