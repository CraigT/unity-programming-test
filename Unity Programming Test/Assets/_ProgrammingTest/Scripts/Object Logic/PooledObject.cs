﻿using UnityEngine;

/********************************************
*
*	Simple base logic class for any object
*	that gets spawned in an object pool.
*
********************************************/

public abstract class PooledObject : MonoBehaviour {

	[HideInInspector]
	public ObjectPool objectPool;
	[HideInInspector]
	public string prefabName;

	public virtual void OnPoolEntered(ObjectPool _pool) {
		objectPool = _pool;
		gameObject.SetActive(false);
	}

	public virtual void OnPoolExited() {
		gameObject.SetActive(true);
	}

	public virtual void ReturnToPool() {
		if (objectPool != null) {
			objectPool.ReturnObjectToPool(this);
		}
	}
}
